(function () {

    function parseMatchData(matchRows) {
        const matches = Array.prototype.map.call(matchRows, row => {
            const time = row.querySelector('td.timespan').textContent;
            return {
                time: time,
                game: row.querySelector('td.name').textContent,
                rank: window.parseInt(row.querySelector('td.rank').textContent),
                gameTime: getGameTime(time),
            };
        });
        return matches.reverse();
    }

    function getGameTime(timeString) {
        const matches = timeString.match(/(\d+) (second|minute|hour|day|week|month|year)s?/i);
        const timeAmount = Number(matches[1]);
        let gameTime = new Date();
        switch (matches[2]) {
            case 'second':
                gameTime.setSeconds(gameTime.getSeconds() - timeAmount);
                break;
            case 'minute':
                gameTime.setMinutes(gameTime.getMinutes() - timeAmount);
                break;
            case 'hour':
                gameTime.setHours(gameTime.getHours() - timeAmount);
                break;
            case 'day':
                gameTime.setDate(gameTime.getDate() - timeAmount);
                break;
            case 'week':
                gameTime.setDate(gameTime.getDate() - 7 * timeAmount);
                break;
            case 'month':
                gameTime.setMonth(gameTime.getMonth() - timeAmount);
                break;
            case 'year':
                gameTime.setFullYear(gameTime.getFullYear() - timeAmount);
                break;
            default:
                throw new Error('Unexpected time format!');
        }
        return gameTime;
    }

    function getAbsoluteRanks(matches, initialRank) {
        const dummyMatch = {
            "absoluteRank": initialRank,
        };
        return matches.reduce((acc, match) => {
            const last = acc[acc.length - 1];
            const updatedMatch = Object.assign({}, match, {
                "absoluteRank": last.absoluteRank + match.rank,
            });
            return acc.concat([updatedMatch]);
        }, [dummyMatch]).slice(1);
    }

    function getSeasonStart() {
        const now = new Date();
        const seasonStart = new Date('2017-06-01 00:00');
        const nextSeasonStart = new Date(seasonStart);
        nextSeasonStart.setMonth(nextSeasonStart.getMonth() + 2);
        while (nextSeasonStart < now) {
            seasonStart.setMonth(seasonStart.getMonth() + 2);
            nextSeasonStart.setMonth(nextSeasonStart.getMonth() + 2);
        }
        return seasonStart;
    }

    function getLeague(rank) {
        const leagues = ['Cardboard', 'Bronze', 'Silver', 'Gold', 'Platinum', 'Diamond', 'Master'];
        const subDivisions = ['IV', 'III', 'II', 'I'];
        const i = Math.floor(rank / 400);
        const j = Math.floor((rank % 400) / 100);
        return leagues[Math.min(i, leagues.length - 1)] + ' ' + subDivisions[j];
    }

    function isArenaMatch(match) {
        return match.game.match(/arena (1|2|3)/i);
    }

    function getMatchStats(matches) {
        const matchesByArena = matches.reduce((acc, match) => {
            const m = isArenaMatch(match);
            const arenaType = m ? m[1] : 'Custom';
            let updatedMatches = {};
            updatedMatches[arenaType] = acc[arenaType].concat([match]);
            return Object.assign({}, acc, updatedMatches);
        }, { 'Custom': [], '1': [], '2': [], '3': [] });
        return Object.keys(matchesByArena).reduce((acc, arenaType) => {
            const sum = matchesByArena[arenaType].reduce((acc, x) => acc + x.rank, 0);
            let stat = {};
            stat[arenaType] = {
                avg: sum / matchesByArena[arenaType].length,
                count: matchesByArena[arenaType].length,
            };
            return Object.assign({}, acc, stat);
        }, {});
    }

    function drawPointGraph(canvas, matches, currentRank, initialRank) {
        let ctx = canvas.getContext('2d');
        ctx.save();
        ctx.fillStyle = '#fff';
        ctx.fillRect(0, 0, canvas.width, canvas.height);

        const avg = matches.reduce((acc, m) => acc + m.absoluteRank, 0) / matches.length;
        ctx.translate(0, avg + canvas.height / 2);

        ctx.strokeStyle = '#555';
        ctx.lineWidth = 1;
        ctx.font = '16px serif'
        ctx.fillStyle = '#000';

        ctx.beginPath();
        for (let i = 0; i <= 35; i++) {
            let yGrid = -i * 100;
            ctx.fillText(String(-yGrid), 10, yGrid - 5);
            ctx.moveTo(0, yGrid);
            ctx.lineTo(canvas.width, yGrid);
        }
        ctx.stroke();

        let x = 0, y = -initialRank;
        ctx.beginPath();
        ctx.moveTo(x, y);
        matches.forEach(match => {
            x += 8;
            y += -match.rank;
            ctx.lineTo(x, y);
        });
        ctx.strokeStyle = '#f00';
        ctx.lineWidth = 2;
        ctx.stroke();

        ctx.textAlign = matches.length > 92 ? 'right' : 'left';
        ctx.fillText(String(matches[matches.length - 1].absoluteRank), x, y);

        ctx.restore();
    }

    function Point(x, y) {
        return { x: x, y: y };
    }

    function comparisonGraph(m1, m2) {
        const xIncrement = 4;

        if (m1.length < m2.length) {
            let tmp = m1;
            m1 = m2;
            m2 = tmp;
        }

        let result1 = [], result2 = [];
        let x = 0, i = 0, j = 0;
        while (j < m2.length) {
            if (m1[i].gameTime < m2[j].gameTime) {
                const nextPoint = Point(x, m1[i].absoluteRank);
                result1.push(nextPoint);
                i++;
            } else if (i >= m1.length || m1[i].gameTime > m2[j].gameTime) {
                const nextPoint = Point(x, m2[j].absoluteRank);
                result2.push(nextPoint);
                j++;
            } else {
                const nextPoint1 = Point(x, m1[i].absoluteRank);
                const nextPoint2 = Point(x, m2[j].absoluteRank);
                result1.push(nextPoint1);
                result2.push(nextPoint2);
                i++;
                j++;
            }
            x += xIncrement;
        }
        while (i < m1.length) {
            const nextPoint = Point(x, m1[i].absoluteRank);
            result1.push(nextPoint);
            x += xIncrement;
            i++;
        }
        return [result1, result2];
    }

    function drawComparisonGraph(canvas, player1Data, player2Data) {
        let ctx = canvas.getContext('2d');
        ctx.save();
        ctx.fillStyle = '#fff';
        ctx.fillRect(0, 0, canvas.width, canvas.height);

        const avg = (player1Data.matches.reduce((acc, m) => acc + m.absoluteRank, 0) +
                     player2Data.matches.reduce((acc, m) => acc + m.absoluteRank, 0))
                    / (player1Data.matches.length + player2Data.matches.length);
        ctx.translate(0, avg + canvas.height / 2);

        ctx.strokeStyle = '#555';
        ctx.lineWidth = 1;
        ctx.font = '16px serif'
        ctx.fillStyle = '#000';

        ctx.beginPath();
        for (let i = 0; i <= 35; i++) {
            let yGrid = -i * 100;
            ctx.fillText(String(-yGrid), 10, yGrid - 5);
            ctx.moveTo(0, yGrid);
            ctx.lineTo(canvas.width, yGrid);
        }
        ctx.stroke();

        const [m1, m2] = comparisonGraph(player1Data.matches.filter(isArenaMatch), player2Data.matches.filter(isArenaMatch));

        // player 1
        let x = 0, y = -player1Data.initialRank;
        ctx.beginPath();
        ctx.moveTo(x, y);
        m1.forEach(match => {
            ctx.lineTo(match.x, -match.y);
        });
        ctx.strokeStyle = '#f00';
        ctx.lineWidth = 2;
        ctx.stroke();

        // player 2
        x = 0, y = -player2Data.initialRank;
        ctx.beginPath();
        ctx.moveTo(x, y);
        m2.forEach(match => {
            ctx.lineTo(match.x, -match.y);
        });
        ctx.strokeStyle = '#00f';
        ctx.lineWidth = 2;
        ctx.stroke();

        ctx.restore();
    }

    function switchTo(view) {
        document.querySelectorAll('main > div').forEach(div => {
            div.classList.add('hidden');
        });
        document.querySelector(view).classList.remove('hidden');
        const hasInput = document.querySelector(view).querySelector('input');
        if (hasInput)
            hasInput.focus();
    }

    function getCurrentSeasonMatches(player) {
        const getPages = [
            fetch('./get-page.php?player=' + player + '&page=home'),
            fetch('./get-page.php?player=' + player + '&page=matches'),
        ];
        return Promise.all(getPages)
            .then(pages => Promise.all(pages.map(response => response.text())))
            .then(([profileHtml, matchesHtml]) => {
                const profilePage = new DOMParser().parseFromString(profileHtml, 'text/html');
                const matchesPage = new DOMParser().parseFromString(matchesHtml, 'text/html');

                const profileName = profilePage.querySelector('.profile-name').textContent;
                const rankText = profilePage.querySelector('.rankings .container-content > .main-secondary.large').textContent;
                const leaderboardPositionText = profilePage.querySelector('.rankings .container-content > .main-secondary > .tooltip > .tooltip-msg > .profile').textContent;
                const currentRank = Number(rankText.match(/\d+/)[0]);
                const leaderboardPosition = Number(leaderboardPositionText.match(/\d+/)[0]) + 1;
                const matches = parseMatchData(matchesPage.querySelectorAll('.matches > tbody > .match'));

                const seasonStart = getSeasonStart();
                let currentSeasonMatches = matches.filter(match => match.gameTime >= seasonStart);
                const initialRank = currentSeasonMatches.reduce((rank, match) => rank - match.rank, currentRank);
                return {
                    matches: getAbsoluteRanks(currentSeasonMatches, initialRank),
                    initialRank: initialRank,
                    currentRank: currentRank,
                    profileName: profileName,
                    leaderboardPosition: leaderboardPosition,
                }
            });
    }

    function comparePlayers(player1, player2) {
        switchTo('#loading');
        const canvas = document.querySelector('#graph');
        const getPlayers = [
            getCurrentSeasonMatches(player1),
            getCurrentSeasonMatches(player2),
        ]
        const colors = ['#f00', '#00f', '#070'];
        Promise.all(getPlayers)
            .then(players => {
                drawComparisonGraph(canvas, players[0], players[1]);
                const legend = players.map((player, i) => {
                    return `<div>
                        <div class="square" style="background-color:${colors[i % colors.length]}"></div>
                        <span>${player.profileName}</span>
                    </div>`;
                }).join('');
                document.querySelector('#infoPanel').innerHTML = `<h2>${players[0].profileName} vs. ${players[1].profileName}</h2>
                    ${legend}`;

                switchTo('#dataview');
            }).catch(() => {
                switchTo('#compare');
            });
    }

    function showStats(player) {
        switchTo('#loading');
        getCurrentSeasonMatches(player)
            .then(playerData => {
                const currentSeasonMatches = playerData.matches;
                const initialRank = playerData.initialRank;
                const currentRank = playerData.currentRank;
                const profileName = playerData.profileName;

                let canvas = document.querySelector('#graph');

                const arenaMatches = currentSeasonMatches.filter(isArenaMatch);
                drawPointGraph(canvas, arenaMatches, currentRank, initialRank);

                const matchStats = getMatchStats(currentSeasonMatches);
                const statRows = Object.keys(matchStats).map(key => {
                    const avg = matchStats[key].avg;
                    const color = isNaN(avg) || avg == 0 ? '#aaa' :
                                avg > 0  ? '#0a0' :
                                            '#f00';
                    const cellValue = isNaN(avg) ? '<b>-</b>' : avg.toFixed(2);
                    const cells = `<td>${key}</td>
                                        <td>${matchStats[key].count}</td>
                                        <td style="color:${color}">${cellValue}</td>`;
                    return `<tr>${cells}</tr>`;
                }).join('');
                document.querySelector('#infoPanel').innerHTML = `<h2>${profileName}'s stats</h2>
                    <table>
                        <thead>
                            <tr>
                                <th>Arena</th>
                                <th>Matches played</th>
                                <th>Average gain</th>
                            </tr>
                        </thead>
                        <tbody>
                            ${statRows}
                        </tbody>
                    </table>
                    <p>Current rank: ${currentRank} (<b>${getLeague(currentRank)}</b>)</p>
                    <p>Leaderboard position: <b>#${playerData.leaderboardPosition}</b></p>`;

                switchTo('#dataview');
            })
            .catch(() => {
                switchTo('#search');
            })
        ;
    }

    window.addEventListener('load', () => {
        document.querySelector('#playerForm').addEventListener('submit', evt => {
            evt.preventDefault();
            const player = document.querySelector('#playerInput').value;
            showStats(player);
        });
        document.querySelector('#comparePlayersForm').addEventListener('submit', evt => {
            evt.preventDefault();
            const player1 = document.querySelector('#player1Input').value;
            const player2 = document.querySelector('#player2Input').value;
            comparePlayers(player1, player2);
        });

        document.querySelector('#searchPlayer').addEventListener('click', evt => {
            switchTo('#search');
        });

        document.querySelector('#comparePlayers').addEventListener('click', evt => {
            switchTo('#compare');
        });
    });

})();
