<?php

ob_start();

$username = urlencode($_GET['player']);
$page = urlencode($_GET['page']);

if ($page !== 'matches' && $page !== 'home') {
    http_response_code(404);
    echo json_encode([
        'error' => 'Invalid page request!'
    ]);
} else {
    $url = sprintf('http://curvefever.io/user/%s/%s/', $username, $page);
    echo file_get_contents($url);
}

ob_end_flush();
